increase_NSC_isolation_level = {
	if = {
		limit = {
			religion = bulwari_sun_cult
			NOT = { isolationism = 4 }		#not highest isolation level for religion
		}
		custom_tooltip = nsc_increase_isolation_tt	#Tooltip for specific localization for each religion
		hidden_effect = { add_isolationism = 1 }
	}
}

decrease_NSC_isolation_level = {
	if = {
		limit = {
			religion = bulwari_sun_cult
			isolationism = 1				#at least one level above lowest isolation for religion
		}
		custom_tooltip = nsc_decrease_isolation_tt
		hidden_effect = { add_isolationism = -1 }
	}
}


increase_runelink_level = {
	if = {
		limit = {
			religion = runefather_worship
			NOT = { isolationism = 9 }
		}
		custom_tooltip = rune_increase_isolation_tt
		hidden_effect = { add_isolationism = 1 }
	}
}

decrease_runelink_level = {
	if = {
		limit = {
			religion = runefather_worship
			isolationism = 6
		}
		custom_tooltip = rune_decrease_isolation_tt
		hidden_effect = { add_isolationism = -1 }
	}
}
