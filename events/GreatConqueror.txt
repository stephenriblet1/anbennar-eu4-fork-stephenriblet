
namespace = great_conqueror

country_event = {
	id = great_conqueror.0
	title = great_conqueror.0.t
	desc = great_conqueror.0.d
	picture = ORTHODOX_eventPicture

	trigger = {
		has_dlc = "Rights of Man"
		is_subject = no
		ai = yes
	}
	
	hidden = yes
	is_triggered_only = yes
	
	immediate = {
		if = {
			limit = { has_country_flag = previous_ruler_was_great_conqueror_flag }
			clr_country_flag = previous_ruler_was_great_conqueror_flag
		}
	
		if = {
			limit = { has_country_flag = great_conqueror_flag }
			REB = { subtract_variable = { nbGC = 1 } }
			clr_country_flag = great_conqueror_flag
			set_country_flag = previous_ruler_was_great_conqueror_flag
			export_to_variable = {
				which = ownDev
				value = development
				who = ROOT
			}
			log = "GREAT CONQUEROR ---------------- [Root.GetName] |[Root.ownDev.GetValue]| stop being a Great Conqueror in [GetYear] [GetMonth]. It's a [Root.GovernmentName] | Number of GC: [REB.nbGC.GetValue]"
		}
	}

	option = {
		name = great_conqueror.0.a
		ai_chance = { 
			factor = 10
			
			modifier = {
				factor = 0
				OR = {
					REB = { check_variable = { nbGC = 10 } }										#No more than 10 Great Conqueror in the world
					is_bankrupt = yes																#Can't be bankrupt bitch
					AND = {
						any_neighbor_country = { ai = no }											#Doesn't target AI next to player before 1550
						NOT = { is_year = 1550 }
					}
					NOT = { has_country_flag = previous_ruler_was_great_conqueror_flag }			#Can't be Great Conqueror 2 time in a row
					coalition_target = ROOT															#Must not be the coalition target
					NOT = { num_of_cities = 2 }														#Must have more than one province
				}
			}
			
			modifier = {
				factor = 1.5
				is_great_power = yes
			}
			
			modifier = {
				factor = 1.5
				MIL = 3 ADM = 3 DIP = 3
			}
			modifier = {
				factor = 2
				MIL = 4 ADM = 4 DIP = 4
			}
			modifier = {
				factor = 2.5
				MIL = 5 ADM = 5 DIP = 5
			}
			modifier = {
				factor = 3
				MIL = 6 ADM = 6 DIP = 6
			}
			
			modifier = {
				factor = 3
				NOT = { ruler_age = 30 }
			}
			
			modifier = {
				factor = 1.5
				ruler_has_personality = mage_personality
			}
			
			modifier = {
				factor = 8
				REB = { NOT = { check_variable = { nbGC = 2 } } }									#More chance if less than 2 Great Conqueror
			}
			
			modifier = {
				factor = 4
				REB = { NOT = { check_variable = { nbGC = 4 } } }									#More chance if less than 4 Great Conqueror
			}
			
			modifier = {
				factor = 2
				REB = { NOT = { check_variable = { nbGC = 5 } } }									#More chance if less than 5 Great Conqueror
			}
			
			modifier = {
				factor = 2
				capital_scope = { superregion = escann_superregion }
				development = 500
			}
			
			modifier = {																			#Weak neighbours give more chance to become one
				factor = 3
				calc_true_if = {
					all_neighbor_country = {
						NOT = { is_part_of_hre = yes }
						NOT = { alliance_with = ROOT }
						NOT = { is_subject_of = ROOT }
						NOT = { army_strength = { who = ROOT value = 0.4 } }
					}
					amount = 2
				}
			}
			
			modifier = {
				factor = 5
				calc_true_if = {
					all_neighbor_country = {
						NOT = { is_part_of_hre = yes }
						NOT = { alliance_with = ROOT }
						NOT = { is_subject_of = ROOT }
						NOT = { army_strength = { who = ROOT value = 0.4 } }
					}
					amount = 3
				}
			}
			
			modifier = {
				factor = 10
				calc_true_if = {
					all_neighbor_country = {
						NOT = { is_part_of_hre = yes }
						NOT = { alliance_with = ROOT }
						NOT = { is_subject_of = ROOT }
						NOT = { army_strength = { who = ROOT value = 0.4 } }
					}
					amount = 4
				}
			}
			
			modifier = {
				factor = 15
				calc_true_if = {
					all_neighbor_country = {
						NOT = { is_part_of_hre = yes }
						NOT = { alliance_with = ROOT }
						NOT = { is_subject_of = ROOT }
						NOT = { army_strength = { who = ROOT value = 0.4 } }
					}
					amount = 5
				}
			}
			
			modifier = {
				factor = 20
				calc_true_if = {
					all_neighbor_country = {
						NOT = { is_part_of_hre = yes }
						NOT = { alliance_with = ROOT }
						NOT = { is_subject_of = ROOT }
						NOT = { army_strength = { who = ROOT value = 0.4 } }
					}
					amount = 6
				}
			}
			
			modifier = {
				factor = 2
				all_neighbor_country = {
					NOT = { tech_difference = 1 }													#More chance to become one if technologically advanced compared to all neighbor
				} 
			}
			
			modifier = {
				factor = 5
				is_revolution_target = yes
			}
			
			modifier = {
				factor = 3
				power_projection = 50
			}
		}
		add_ruler_personality = great_conqueror_personality
		add_ruler_modifier = {
			name = great_conqueror_modifier
			duration = -1
		}
		set_ai_personality = {
			personality = ai_militarist
			locked = yes
		}
		add_stability = 1
		change_dip = 1
		change_adm = 1
		change_mil = 1
		
		# Back-end
		set_country_flag = great_conqueror_flag
		REB = { change_variable = { nbGC = 1 } }
		export_to_variable = {
			which = ownDev
			value = development
			who = ROOT
		}
		log = "GREAT CONQUEROR ---------------- [Root.GetName] |[Root.ownDev.GetValue]| became a Great Conqueror in [GetYear] [GetMonth]. It's a [Root.GovernmentName] | Number of GC: [REB.nbGC.GetValue]"
	}
	
	option = {
		name = great_conqueror.0.b
		ai_chance = { 
			factor = 95
			
			modifier = {
				factor = 1.5
				overextension_percentage = 0.5
			}
			
			modifier = {
				factor = 1.5
				average_effective_unrest = 1
			}
			
			modifier = {
				factor = 1.5
				num_of_loans = 5
			}
			
			modifier = {
				factor = 1.5
				corruption = 5
			}
			
			modifier = {
				factor = 1.5
				NOT = { stability = 0 }
			}
			
			modifier = {
				factor = 10
				government = republic
			}
			
			modifier = {
				factor = 10
				is_subject = yes
			}
		}
	}
}
