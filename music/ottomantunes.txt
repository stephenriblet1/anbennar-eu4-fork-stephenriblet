# Kairi's Soundtrack 3 - Ottoman Tunes 
# Songs to listen to while playing in the Near East
# BULWAR


song = {
	name = "Faith_Restored"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = bulwari }
			NOT = { religion_group = kheteratan }
		}
		modifier = {
			factor = 2
			culture_group = bulwari
			culture_group = divenori
			culture = sun_elf
			culture = desert_elf
		}
	}
}

song = {
	name = "Homebound"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = bulwari }
			NOT = { religion_group = kheteratan }
		}
		modifier = {
			factor = 2
			culture_group = bulwari
			culture_group = divenori
			culture = sun_elf
			culture = desert_elf
		}
	}
}

song = {
	name = "Peace_Cannot_Last"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = bulwari }
			NOT = { religion_group = kheteratan }
		}
		modifier = {
			factor = 2
			culture_group = bulwari
			culture_group = divenori
			culture = sun_elf
			culture = desert_elf
		}
	}
}

song = {
	name = "Sundered_Hills"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = bulwari }
			NOT = { religion_group = kheteratan }
		}
		modifier = {
			factor = 2
			culture_group = bulwari
			culture_group = divenori
			culture = sun_elf
			culture = desert_elf
		}
	}
}

song = {
	name = "Whispers_in_the_Dark"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = bulwari }
			NOT = { religion_group = kheteratan }
		}
		modifier = {
			factor = 2
			culture_group = bulwari
			culture_group = divenori
			culture = sun_elf
			culture = desert_elf
		}
	}
}